"""Launch bot."""
from musicbot.bot import Bot
from musicbot import config
import logging
from musicbot.utils import create_gettext_func


if __name__ == "__main__":
    formatter = logging.Formatter(
        fmt="[%(asctime)s] %(levelname)s"
            "[%(name)s.%(funcName)s:%(lineno)d] %(message)s"
    )
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    cfg = config.load_config()

    _ = create_gettext_func(cfg["lang"])
    bot = Bot(cfg)

    if cfg["token"] == "":
        raise ValueError(_("No token has been provided."))
    bot.run(cfg["token"])
