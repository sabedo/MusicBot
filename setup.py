from setuptools import setup

setup(name='musicbot',
      version='2.0',
      description='Python Music Bot',
      url='https://gitlab.com/sabedo/MusicBot',
      install_requires=[
            'discord==2.2.2',
            'toml==0.10.2',
            'yt_dlp==2023.3.4',
            'PyNaCl==1.5.0',
            'static-ffmpeg==2.5',
            'python-gettext==5.0'
        ],
      packages=['musicbot', 'musicbot/cogs'],
      package_dir={'musicbot': 'src/musicbot',
                   'musicbot/cogs': 'src/musicbot/cogs'},
      entry_points={"console_scripts": ["musicbot = musicbot:main"]}
      )
