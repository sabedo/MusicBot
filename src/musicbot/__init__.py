"""Create logger and token."""
import logging
from . import config
from bot import Bot


def main():
    logging.Formatter(
        fmt="[%(asctime)s] %(levelname)s"
            "[%(name)s.%(funcName)s:%(lineno)d]%(message)s"
    )
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    cfg = config.load_config()
    print(cfg)

    bot = Bot(cfg)

    if cfg["token"] == "":
        raise ValueError("No token has been provided.")
    bot.run(cfg["token"])


if __name__ == "__main__":
    main()
