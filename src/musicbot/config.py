"""Create config."""
import logging
import os
import toml

EXAMPLE_CONFIG = """\"token\"=\"\" # the bot's token
\"lang\"=\"ru\"
\"prefix\"=\"!\" # prefix used to denote commands
"""


def load_config(path: str = "./config.toml"):
    """Load the config from path."""
    if os.path.exists(path) and os.path.isfile(path):
        config = toml.load(path)
        return config
    else:
        with open(path, "w") as config:
            config.write(EXAMPLE_CONFIG)
            logging.warn(
                f"No config file found. Create a default config file at {path}"
            )
        return load_config(path=path)
