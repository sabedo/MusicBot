"""Create utils."""
import discord
import logging
import gettext


def create_logger():
    """Create logging."""
    formatter = logging.Formatter(
        fmt="[%(asctime)s] %(levelname)s"
            "[%(name)s.%(funcName)s:%(lineno)d] %(message)s"
    )
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)


def create_gettext_func(lang: str):
    """Create language function."""
    language_translations = gettext.translation(
        "base", "locales", languages=[lang])
    language_translations.install()

    return language_translations.gettext


async def remove_reaction(reactions: list[discord.reaction.Reaction],
                          emoji_name: str, member: discord.member) -> None:
    """
    Delete reactions after clicking on them.

    Parameters
    ------------
    reactions: :class:`list[discord.reaction.Reaction]`
        The all reactions of the message.
    emoji_name: :class:`str`
        The emoji name.
    member: :class:`discord.member`
        The user who clicked on the emoji.

    Returns
    --------
    None
    """
    for reaction in reactions:
        if reaction.emoji == emoji_name:
            await reaction.remove(member)
