import discord
from discord.ext import commands
import os
import logging


class Bot(commands.Bot):
    """Create functions."""

    def __init__(self, cfg):
        """Init."""
        super().__init__(
            command_prefix=commands.when_mentioned_or(cfg["prefix"]),
            intents=discord.Intents.all(),
            help_command=commands.DefaultHelpCommand(dm_help=True)
        )

    async def setup_hook(self):
        """Set up hook."""
        cogs_folder = f"{os.path.abspath(os.path.dirname(__file__))}\\cogs"
        for filename in os.listdir(cogs_folder):
            if filename.endswith(".py"):
                await super().load_extension(f"musicbot.cogs.{filename[:-3]}")
        await super().tree.sync()
        logging.info("Loaded cogs")
