"""Create music function."""
import asyncio
import discord
from ..config import load_config
from ..utils import remove_reaction, create_gettext_func
from discord import FFmpegPCMAudio, VoiceChannel, RawReactionActionEvent
from discord.ext import commands
from yt_dlp import YoutubeDL
import logging
import copy


class Music(commands.Cog):
    """Bot commands to search music."""

    def __init__(self, bot):
        """Init options."""
        self.bot = bot

        self.YDL_OPTIONS = {
            'format': 'bestaudio/best',
            'noplaylist': True
        }
        self.FFMPEG_OPTIONS = {
            'before_options': '-reconnect 1 -reconnect_streamed 1'
            ' -reconnect_delay_max 5',
            'options': '-bufsize 16M',
        }
        self.MUSIC_EMOJI = {"play": "▶️", "stop": "⏸️", "next_track": "⏭️"}

        self.queue = []
        self.vc: VoiceChannel = None
        self.player = None
        self._is_playing = False
        self._is_paused = False
        self.ctx: commands.Context = None

        cfg = load_config()
        self._ = create_gettext_func(cfg["lang"])

    async def create_music(self, ctx: commands.Context, text: str) -> None:
        """
        Create a message with information about the track being played.

        Parameters
        ------------
        ctx: :class:`commands.Context`
            The command context.
        text: :class:`str`
            The text of the message.

        Returns
        ------------
        None
        """
        message = await ctx.send(text)
        for emoji in self.MUSIC_EMOJI.values():
            await message.add_reaction(emoji)

    def update_context(self, payload: discord.RawReactionActionEvent):
        """Update our context with the user who reacted."""
        ctx = copy.copy(self.ctx)
        ctx.author = payload.member

        return ctx

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: RawReactionActionEvent):
        """
        Start events when a user adds an emote reaction to a message.

        Parameters
        ------------
        payload:
            :class:
            `discord.RawReactionActionEvent`
            The raw reaction addition event payload.

        Returns
        ------------
        None
        """
        channel: discord.channel.TextChannel = self.bot.get_channel(
            payload.channel_id)
        message: discord.message.Message = await channel.fetch_message(
            payload.message_id)
        member = payload.member

        if member.bot:
            return

        logging.info(f"Reaction: {payload.emoji}")
        emojies = self.MUSIC_EMOJI

        if payload.emoji.name == emojies["stop"]:
            if self._is_playing:
                self._pause()
        elif payload.emoji.name == emojies["play"]:
            if self._is_paused:
                self._pause()
        elif payload.emoji.name == emojies["next_track"]:
            if self._is_paused:
                self._pause()

            ctx = self.update_context(payload)
            await self._skip(ctx)
        await remove_reaction(message.reactions, payload.emoji.name, member)

    def search_yt(self, url: str) -> dict[str, str]:
        """
        Search for videos on YouTube.

        Parameters
        ------------
        url: :class:`str`
            The URL where you need to find the video.

        Returns
        ------------
        :class:`dict[str, str]`
        """
        with YoutubeDL(self.YDL_OPTIONS) as ydl:
            try:
                info = ydl.extract_info(
                    "ytsearch:%s" % url, download=False)['entries'][0]
                for track in info['formats']:
                    if ("filesize" in track and 'format_note' in track
                            and track["format_note"] == "low"):
                        self.queue.append(
                            {'source': track['url'], 'title': info['title']})
                        return
            except Exception:
                logging.error(self._(
                    "No available audio format found."
                    "The default format is used."))
                return False
        self.queue.append(
            {'source': info['formats'][3]['url'], 'title': info['title']})

    async def connect(self, ctx: commands.Context) -> None:
        """
        Join the user who called the command.

        Parameters
        ------------
        ctx: :class:`commands.Context`
            The command context.

        Returns
        ------------
        None
        """
        if not ctx.guild.voice_client:
            self.vc = await ctx.author.voice.channel.connect()
        else:
            self.vc = ctx.guild.voice_client

    async def _play_loop(self, ctx: commands.Context):
        self._is_playing = True
        playing = self.vc.is_playing()
        while playing:
            await asyncio.sleep(1)
            playing = self.vc.is_playing()

        if not self._is_paused and len(self.queue) > 0:
            self.queue.pop(0)
            await self._play_music(ctx)
        else:
            self._is_playing = False

    async def _play_music(self, ctx: commands.Context) -> None:
        """
        Command to play music via ffmpeg.

        Parameters
        ----------
        ctx: :class: `commands.Context`
            The command context.

        Returns
        ------------
        None
        """
        self.ctx = ctx

        async def next_music(err):
            if len(self.queue) > 0:
                await self._play_music(ctx)

        if len(self.queue) == 0:
            await self._leave(ctx)
            return

        if self._is_paused:
            logging.info(self._("Unpaused. Now playing: ")
                         + f"{self.queue[0]['title']}")
            self._is_playing = False
            self._is_paused = True
            self.vc.resume()
            await self._play_loop(ctx)
            return

        # not in pause
        song = self.queue[0]
        m_url = song['source']

        logging.info(self._("Now playing: ")
                     + f"{song['title']}")

        self.vc.play(
            FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS),
            after=next_music)
        await self.create_music(ctx, self._("Now playing: ")
                                + f"{song['title']}")
        await self._play_loop(ctx)

    @commands.command()
    @commands.guild_only()
    async def play(self, ctx: commands.Context, url=None) -> None:
        """
        Command available to the bot is designed to check.

        Parameters
        ------------
        ctx: :class:`commands.Context`
            The command context.
        url: :class:`str`=None
            URL for video search.

        Returns
        ------------
        None
        """
        self.ctx = ctx
        try:
            if ctx.author.voice.channel:
                await self.connect(ctx)

                if url is not None:
                    logging.info(self._("Finding url: ") + f"{url}")
                    self.search_yt(url)

                if self._is_paused:
                    self._pause()
                    return
                await self._play_music(ctx)
            else:
                await ctx.send(self._("Please connect to a voice channel."))
        except Exception as e:
            self._is_playing = False
            logging.error(e)

    @commands.command(
            name="pause", help="Pauses the current song being played")
    async def pause(self, ctx: commands.Context, *args) -> None:
        """
        Pause command to play music.

        Parameters
        ------------
        ctx: :class:`commands.Context`
            The command context.

        Returns
        ------------
        None
        """
        logging.info(self._("Music is stopped."))
        self._pause()

    def _pause(self):
        if self._is_playing:
            self._is_playing = False
            self._is_paused = True
            self.vc.pause()
        elif self._is_paused:
            self._is_paused = False
            self._is_playing = True
            self.vc.resume()

    async def _leave(self, ctx: commands.Context):
        client = ctx.guild.voice_client
        if client and client.channel:
            self.vc.stop()
            await client.disconnect()
            self.queue = []
            self.is_playing = False
            self._is_paused = False
        else:
            raise commands.CommandError("Not in a voice channel.")

    @commands.command()
    @commands.guild_only()
    async def leave(self, ctx: commands.Context):
        """Leave from channel."""
        await self._leave(ctx)
        logging.info(self._("Leave from channel."))

    @commands.command()
    @commands.guild_only()
    async def clean(self, ctx: commands.Context):
        """Clean queue list."""
        self.queue = []
        await self._leave(ctx)
        logging.info(self._("Delete all music from queue."))

    @commands.command()
    @commands.guild_only()
    async def list(self, ctx: commands.Context):
        """Get list with all musics in queue."""
        logging.info(self._("Get all musics from queue."))

        if len(self.queue) == 0:
            await ctx.send("The play queue is empty.")
            return

        command_list = [f"{len(self.queue)} songs in queue: "]
        for (index, song) in enumerate(self.queue):
            command_list += [f"  {index + 1} {song['title']}"]
        command_list = "\n".join(command_list)
        await ctx.send(command_list)

    async def _skip(self, ctx: commands.Context):
        self.vc.stop()
        if len(self.queue):
            self.queue.pop(0)
            await self._play_music(ctx)

    @commands.command()
    @commands.guild_only()
    async def skip(self, ctx: commands.Context):
        """Skip music and play next."""
        logging.info(self._("Skip music and play next."))
        await self._skip(ctx)


async def setup(bot: commands.Bot):
    """Set up."""
    await bot.add_cog(Music(bot))
