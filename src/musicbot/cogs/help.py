"""Create and set up help."""
from discord.ext import commands
from ..config import load_config
from ..utils import create_gettext_func


class Help(commands.Cog):
    """Bot commands to help for user."""

    def __init__(self, bot: commands.Bot):
        """Init load config."""
        self.bot = bot

        cfg = load_config()
        self._ = create_gettext_func(cfg["lang"])

    @commands.command()
    @commands.guild_only()
    async def help(self, ctx: commands.Context, command: str = ""):
        """Help."""
        prefix = ctx.prefix

        all_commands = {
            f"{prefix}help":
                self._("Display the list of commands"
                       " or the help arguments of command"),
            f"{prefix}play":
                self._("The command to play music via ffmpeg."),
            f"{prefix}pause":
                self._("Pause command to play music."),
            f"{prefix}skip":
                self._("Skip music and play next."),
            f"{prefix}leave":
                self._("Leave from channel."),
            f"{prefix}list":
                self._("Get list with all musics in queue."),
            f"{prefix}clean":
                self._("Delete all music from queue."),
        }
        if command == "":
            await ctx.channel.send("\n".join(
                ": ".join(item) for item in all_commands.items()
                ))
        elif f"{prefix + command}" in all_commands.keys():
            await ctx.channel.send(
                f"{prefix + command}: {all_commands[prefix + command]}")
        else:
            await ctx.channel.send(self._("Command not found"))


async def setup(bot: commands.Bot):
    """Set up help."""
    bot.remove_command("help")
    await bot.add_cog(Help(bot))
